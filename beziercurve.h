#ifndef BEZIERCURVE_H
#define BEZIERCURVE_H


#include <QSGGeometryNode>
#include <QQuickItem>
#include <QQmlApplicationEngine>

class BezierKurve : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QPointF p1 READ getP1 WRITE setP1 NOTIFY p1Changed)
    Q_PROPERTY(QPointF p2 READ getP2 WRITE setP2 NOTIFY p2Changed)
    Q_PROPERTY(QPointF p3 READ getP3 WRITE setP3 NOTIFY p3Changed)
    Q_PROPERTY(QPointF p4 READ getP4 WRITE setP4 NOTIFY p4Changed)
    Q_PROPERTY(int segmentCount READ getSegmentCount WRITE setSegmentCount NOTIFY segmentCountChanged)
    // QML_ELEMENT

public:
    BezierKurve(QQuickItem *parent = nullptr);
    ~BezierKurve();

    QSGNode *updatePaintNode(QSGNode *, UpdatePaintNodeData *);

    QPointF getP1() const { return m_p1; }
    QPointF getP2() const { return m_p2; }
    QPointF getP3() const { return m_p3; }
    QPointF getP4() const { return m_p4; }

    int getSegmentCount() const { return m_segmentCount; }

    void setP1(const QPointF &p);
    void setP2(const QPointF &p);
    void setP3(const QPointF &p);
    void setP4(const QPointF &p);

    void setSegmentCount(int count);

signals:
    void p1Changed(const QPointF &p);
    void p2Changed(const QPointF &p);
    void p3Changed(const QPointF &p);
    void p4Changed(const QPointF &p);

    void segmentCountChanged(int count);

private:
    QPointF m_p1;
    QPointF m_p2;
    QPointF m_p3;
    QPointF m_p4;

    int m_segmentCount;
};

#endif // BEZIERCURVE_H
