#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "miditouch.h"
#include "keylayout.h"
#include "midi/midioutputmanager.h"
#include "midi/RtMidi.h"
#include "beziercurve.h"
#include <iostream>

MidiOutputManager* midiOut=nullptr;
//MidiInputManager*  midiIn=nullptr;

int main(int argc, char *argv[])
{

    unsigned int midiPort = 0;
    try {
        midiOut = new MidiOutputManager(0);
        midiOut->printMidiOutputPorts();
        midiOut->setPort(midiPort);
        midiOut->openPort();
    } catch (RtMidiError& e) {
        e.printMessage();
    }

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qmlRegisterType<KeyLayout>("KeyLayout.MIDItaction" , 1, 0, "KeyLayout");
    qmlRegisterType<MidiTouch>("MidiTouch.MIDItaction" , 1, 0, "TouchEvent");
    qmlRegisterType<BezierKurve>("BezierCurve.MIDItaction",1,0, "Bezier");
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    //midiOut->closePort();
    return app.exec();
}
