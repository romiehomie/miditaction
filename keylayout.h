#ifndef KEYLAYOUT_H
#define KEYLAYOUT_H

#include <QQuickItem>

// Qt includes
#include <QColor>
// standard library includes
#include <QGraphicsScene>
#include <QSGGeometryNode>
#include <vector>

#include "clipper/clipper.h"
#include "voronoi/jc_voronoi.h"
/**
 * @brief The keyButton struct
 *
 * Holds all information to construct a button on the key layout
 * in order to draw the button
 * and to map the touch input position to the frequency of the key
 */
struct KeyButton
{
    int index;
    float freq;
    QColor color;
    float xPos;
    float yPos;
    std::vector<QSGGeometry::Point2D> vertices;
    uint curveSegments;

    KeyButton(){
        vertices = std::vector<QSGGeometry::Point2D>();
    }
    ///////////////////////////////////////////////////////////
    /// \brief KeyButton
    /// \param index - integer counting buttons from 0 upwards
    ///              - commonly set to the index of the
    ///              - keyButtonList member of the KeyLayout Class
    /// \param freq  - of note
    /// \param color - of button
    /// \param xPos  - of button center
    /// \param yPos  - of button center
    ///
    /// Constructor
    ///
    KeyButton(int index, float freq, QColor color, float xPos, float yPos, uint curveSegments)
    : index(index), freq(freq), color(color), xPos(xPos), yPos(yPos)
    , curveSegments(curveSegments){
        vertices = std::vector<QSGGeometry::Point2D>();
    }
};


class KeyLayout : public QQuickItem
{
    Q_OBJECT

    // key layout boundaries
    bool fullScreen;
    uint width;
    uint height;
    uint leftMargin;
    uint rightMargin;
    uint topMargin;
    uint bottomMargin;

    // key layout
    uint keysPerRow;
    uint keysPerCol;
    uint numKeyBtns;

    // key to frequency mapping
    float TWELTH_ROOT_OF_TWO;
    uint widthStep;
    uint heightJump;
    float intervStep;
    float intervJump;
    float fMin;
    float fRoot;
    std::vector<float> freqList;

    // Button curvature, color, border
    float curved;
    uint curveSegments;
    int padding;
    int borderWidth;

    //
    jcv_point* keyPositions = nullptr;
    jcv_diagram diagram;

    //
    QGraphicsScene *mScene; // scene to paint the preview on
    std::vector<KeyButton> keyButtonList; // will be generated from user input

public:
    KeyLayout(QQuickItem *parent = nullptr);
    ~KeyLayout();
    void load();
    void save();
    float getRelativePosX(float fXLeftMrgin, float fXRightMargin, float f);
    void setupRowsCols();
    void setupFrequencies();
    void setupVoronois();

    void setupKeyButtonList();
    QSGNode* updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *);
    void doThatStuff(int ii, int jj, int kk, ClipperLib::Paths solution);
signals:

public slots:
};

#endif // KEYLAYOUT_H
