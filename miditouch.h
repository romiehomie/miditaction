#ifndef MIDITOUCH_H
#define MIDITOUCH_H

#include "midi/midioutputmanager.h"

#include <QObject>
#include <QRectF>

extern MidiOutputManager* midiOut;

class MidiTouch : public QObject
{
    Q_OBJECT

    int posX;
    Q_PROPERTY(int posX READ getPosX WRITE setPosX)
    int posY;
    Q_PROPERTY(int posY READ getPosY WRITE setPosY)
    double velX;
    Q_PROPERTY(double velX READ getVelX WRITE setVelX)
    double velY;
    Q_PROPERTY(double velY READ getVelY WRITE setVelY)
    QRectF area;
    Q_PROPERTY(QRectF area READ getArea WRITE setArea) // NOTIFY areaChanged)
    double pressure;
    Q_PROPERTY(double pressure READ getPressure WRITE setPressure)
    int pointId;
    Q_PROPERTY(int pointId READ getPointId WRITE setPointId) // NOTIFY pointIdChanged)
    bool pressed;
    Q_PROPERTY(bool pressed READ isPressed() WRITE setPressed())

public:

    //////////////////////////////////////////
    /// \brief MidiTouch
    /// \param _parent
    ///
    MidiTouch(QObject *_parent=nullptr);

    void playMidi();

    //////////////////////////////////////////
    /// \brief getPosX
    /// \return
    ///
    int getPosX();

    //////////////////////////////////////////
    /// \brief getPosY
    /// \return
    ///
    int getPosY();

    //////////////////////////////////////////
    /// \brief getArea
    /// \return
    ///
    QRectF getArea();

    //////////////////////////////////////////
    /// \brief getVelX
    /// \return
    ///
    /// Not all touch devices support velocity. If velocity is not supported,
    /// it will be reported as 0,0.
    /// velocity is a vector with magnitude reported in pixels per second.
    ///
    double getVelX();

    //////////////////////////////////////////
    /// \brief getVelY
    /// \return
    ///
    /// Not all touch devices support velocity. If velocity is not supported,
    /// it will be reported as 0,0.
    /// velocity is a vector with magnitude reported in pixels per second.
    ///
    double getVelY();

    //////////////////////////////////////////
    /// \brief getPressure
    /// \return
    ///
    /// pressure is a value in the range of 0.0 to 1.0.
    ///
    double getPressure();

    //////////////////////////////////////////
    /// \brief getPointId
    /// \return
    ///
    int getPointId();

    //////////////////////////////////////////
    /// \brief isPressed
    /// \return true - if touch starts
    ///         false - if touch stops
    ///
    bool isPressed();

    //////////////////////////////////////////
    /// \brief setPosX
    /// \param posX
    ///
    void setPosX(int posX);

    //////////////////////////////////////////
    /// \brief setPosY
    /// \param posY
    ///
    void setPosY(int posY);

    //////////////////////////////////////////
    /// \brief setArea
    ///
    void setArea(QRectF);

    //////////////////////////////////////////
    /// \brief setVelX
    /// \param velX
    ///
    /// Not all touch devices support velocity. If velocity is not supported,
    /// it will be reported as 0,0.
    /// velocity is a vector with magnitude reported in pixels per second.
    ///
    void setVelX(double velX);

    //////////////////////////////////////////
    /// \brief setVelY
    /// \param velY
    ///
    /// Not all touch devices support velocity. If velocity is not supported,
    /// it will be reported as 0,0.
    /// velocity is a vector with magnitude reported in pixels per second.
    ///
    void setVelY(double velY);

    //////////////////////////////////////////
    /// \brief setPressure
    /// \param pressure
    ///
    void setPressure(double pressure);

    //////////////////////////////////////////
    /// \brief setPointId
    /// \param pointId
    ///
    void setPointId(int pointId);

    //////////////////////////////////////////
    /// \brief setPressed
    /// \param pressed
    ///
    void setPressed(bool pressed);
};

#endif // MIDITOUCH_H
