import QtQuick 2.12
import QtQuick.Controls 2.5
import Qt.labs.settings 1.0

Page {
    id: mainPage
    width: 800
    height: 600

    title: qsTr("Key Layout Settings")

    Grid {
        id: grid
        width: 800
        height: 600
        spacing: 3
        columns: 8
        anchors.fill: parent

        Text {
            id: lblLeftMargin
            color: "#ffffff"
            text: qsTr("  left margin / [px]")
            font.pixelSize: 12
        }

        TextField {
            id: teLeftMargin
            width: 90
            text: qsTr("0")
            inputMask: "000"
        }

        Text {
            id: lblRightMargin
            color: "#ffffff"
            text: qsTr("  right margin / [px]")
            font.pixelSize: 12
        }


        TextField {
            id: tfRightMargin
            width: 90
            text: qsTr("0")
            inputMask: "000"
        }

        Text {
            id: lblTopMargin
            color: "#ffffff"
            text: qsTr("  top margin / [px]")
            font.pixelSize: 12
        }

        TextField {
            id: tfTopMargin
            width: 90
            text: qsTr("0")
            inputMask: "000"
        }

        Text {
            id: lblBottomMargin
            color: "#ffffff"
            text: qsTr("  bottom margin / [px]")
            font.pixelSize: 12
        }

        TextField {
            id: tfBottomMargin
            width: 90
            text: qsTr("0")
            inputMask: "000"
        }

        Text {
            id: lblLayoutW
            color: "#ffffff"
            text: qsTr("  width / [px]")
            font.pixelSize: 12
            }

        TextField {
            id: tfLayoutW
            width: 90
            text: qsTr("1000")
            inputMask: "0000"
        }

        Text {
            id: lblLayoutH
            color: "#ffffff"
            text: qsTr("  height / [px]")
            font.pixelSize: 12
        }

        TextField {
            id: tfLayoutH
            width: 90
            text: qsTr("1000")
            inputMask: "0000"
        }

        Text {
            id: lblWiStep
            color: "#ffffff"
            text: qsTr("  width step / [px]")
            font.pixelSize: 12
        }

        TextField {
            id: tfWiStep
            width: 90
            text: qsTr("100")
             inputMask: "0000"
        }

        Text {
            id: lblHiJump
            color: "#ffffff"
            text: qsTr("  height jump / [px]")
            font.pixelSize: 12
        }

        TextField {
            id: tfHiJump
            width: 90
            text: qsTr("80")
            inputMask: "0000"
        }

        Text {
            id: lblFMin
            width: 90
            text: qsTr("fMin / [Hz]")
            font.pixelSize: 12
        }

        TextField {
            id: tfFMin
            width: 90
            text: qsTr("100.0")
            inputMask: "00000.00"
        }

        Text {
            id: lblFRoot
            color: "#ffffff"
            text: qsTr("  fRoot / [Hz]")
            font.pixelSize: 12
        }

        TextField {
            id: tfFRoot
            width: 90
            text: qsTr("110")
        }

        Text {
            id: lblStep
            color: "#ffffff"
            text: qsTr("  step / [semitones]")
            font.pixelSize: 12
        }

        TextField {
            id: tfStep
            width: 90
            text: qsTr("1.0")
            inputMask:  "00.000000"
        }

        Text {
            id: lblJump
            color: "#ffffff"
            text: qsTr("  jump / [semitones]")
            font.pixelSize: 12
        }

        TextField {
            id: tfJump
            width: 90
            text: qsTr("6.5")
            inputMask: "000.000000"
        }

        Text {
            id: lblCur
            color: "#ffffff"
            text: qsTr("curvature / [%]")
            elide: Text.ElideNone
            wrapMode: Text.NoWrap
            verticalAlignment: Text.AlignTop
            horizontalAlignment: Text.AlignRight
            font.pixelSize: 12
        }

        TextField {
            id: tfCur
            width: 90
            text: qsTr("50")
            inputMask: "000"
        }

        Text {
            id: lblCurSeg
            color: "#ffffff"
            text: qsTr("  curve segments")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            font.pixelSize: 12
        }

        TextField {
            id: tfCurSeg
            width: 90
            text: qsTr("10")
            inputMask: "000"
        }

        Text {
            id: lblPadd
            color: "#ffffff"
            text: qsTr("  padding / [px]")
            elide: Text.ElideRight
            horizontalAlignment: Text.AlignRight
            font.pixelSize: 12
        }

        TextField {
            id: tfPadd
            width: 90
            text: qsTr("5")
            inputMask: "000"
        }

        Text {
            id: lblBorder
            color: "#ffffff"
            text: qsTr("  border width / [px]")
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 12
        }

        TextField {
            id: tfBorder
            width: 90
            text: qsTr("1")
            inputMask: "000";
        }

        Settings {
            id: sts
            property alias width: tfLayoutW.text
            property alias height: tfLayoutH.text
            property alias leftMagin: teLeftMargin.text
            property alias rightMargin: tfRightMargin.text
            property alias topMargin: tfTopMargin.text
            property alias bottomMargin: tfBottomMargin.text
            property alias widthStep: tfWiStep.text
            property alias heightJump: tfHiJump.text
            property alias intervStep: tfStep.text
            property alias intervJump: tfJump.text
            property alias fMin: tfFMin.text
            property alias fRoot: tfFRoot.text
            property alias curved: tfCur.text
            property alias curveSegments: tfCurSeg.text
            property alias padding: tfPadd.text
            property alias borderWidth: tfBorder.text
        }
















    }

}



