import QtQuick 2.12//2.15 has touchpoint rotation
import QtQuick.Controls 2.5
import "content"

import MidiTouch.MIDItaction 1.0
import BezierCurve.MIDItaction 1.0
Page{

    Bezier {
            id: line
            anchors.fill: parent
            anchors.margins: 20
            property real t
            SequentialAnimation on t {
                NumberAnimation { to: 1; duration: 2000; easing.type: Easing.InOutQuad }
                NumberAnimation { to: 0; duration: 2000; easing.type: Easing.InOutQuad }
                loops: Animation.Infinite
            }
            p1: Qt.point(1,1)
            p2: Qt.point(t, 1 - t)
            p3: Qt.point(1 - t, t)
            p4: Qt.point(0,0)
        }

    Bezier {
            id: line2
            anchors.fill: parent
            anchors.margins: 20
            property real t
            SequentialAnimation on t {
                NumberAnimation { to: 1; duration: 2000; easing.type: Easing.InOutQuad }
                NumberAnimation { to: 0; duration: 2000; easing.type: Easing.InOutQuad }
                loops: Animation.Infinite
            }
            p1: Qt.point(0,1)
            p2: Qt.point(1-t, 1-t)
            p3: Qt.point(t, t)
            p4: Qt.point(1,0)
        }

    MultiPointTouchArea {
        anchors.fill: parent
        minimumTouchPoints: 1
        maximumTouchPoints: 5
        touchPoints: [
            TouchPoint { id: touch0 },
            TouchPoint { id: touch1 },
            TouchPoint { id: touch2 },
            TouchPoint { id: touch3 },
            TouchPoint { id: touch4 },
            TouchPoint { id: touch5 },
            TouchPoint { id: touch6 },
            TouchPoint { id: touch7 },
            TouchPoint { id: touch8 },
            TouchPoint { id: touch9 }
        ]
    }

    TouchEvent {
        posX: touch0.x
        posY: touch0.y
        area:   touch0.area
        velX: touch0.velocity.x
        velY: touch0.velocity.y
        pressure: touch0.pressure
        pointId: touch0.pointId
        pressed: touch0.pressed

    }

    ParticleFlame {
        color: "red"
        emitterX: touch1.x
        emitterY: touch1.y
        emitting: touch1.pressed
    }
    ParticleFlame {
        color: "green"
        emitterX: touch2.x
        emitterY: touch2.y
        emitting: touch2.pressed
    }
    ParticleFlame {
        color: "yellow"
        emitterX: touch3.x
        emitterY: touch3.y
        emitting: touch3.pressed
    }
    ParticleFlame {
        color: "blue"
        emitterX: touch4.x
        emitterY: touch4.y
        emitting: touch4.pressed
    }
    ParticleFlame {
        color: "violet"
        emitterX: touch0.x
        emitterY: touch0.y
        emitting: touch0.pressed
    }

    Text {
        id: element
        color: "#89cfed"
        text: "MIDItaction (ditaction: detection / taction: touch, contact)"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.fill: parent
        textFormat: Text.AutoText
        opacity: 0.6
        font.pixelSize: 16
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:18;anchors_height:20;anchors_width:95;anchors_x:284;anchors_y:31}
}
 ##^##*/
