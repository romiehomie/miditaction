QT += \
    quick \
    core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    keylayout.cpp \
        main.cpp \
    clipper/clipper.cpp \
    miditouch.cpp \
    midi/midimessage.cpp \
    midi/midioutputmanager.cpp \
    midi/RtMidi.cpp \
    touchtokey.cpp \
    beziercurve.cpp \
    voronoi/utilsVector.cpp

RESOURCES += qmlressources\qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES +=

HEADERS += \
    clipper/clipper.h \
    keylayout.h \
    voronoi/jc_voronoi.h \
    miditouch.h \
    midi/midimessage.h \
    midi/midimessagenames.h \
    midi/RtMidi.h \
    touchtokey.h \
    midi/midioutputmanager.h \
    beziercurve.h \
    beziercurve.h \
    voronoi/utilsVector.h


#for windows rtmidi support
#DEFINES += __WINDOWS_MM__ // now defined in RtMidi.h
LIBS+= -lwinmm
