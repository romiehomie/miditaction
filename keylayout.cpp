#include "keylayout.h"

#define JC_VORONOI_IMPLEMENTATION

// 3rd party includes
#include "clipper/clipper.h" // an open source freeware library for clipping and offsetting lines and polygons
#include "voronoi/jc_voronoi.h" // an open source freeware library for calculating voronoi decomposition
// Qt includes
#include <QDesktopWidget>
// standard library includes
#include <cmath>
#include <vector>
#include <iostream>
#include <QSettings>
#include <QSGGeometryNode>
#include <QSGFlatColorMaterial>
// my includes
#include "voronoi/utilsVector.h"

KeyLayout::KeyLayout(QQuickItem *parent)
    : QQuickItem(parent){
    qDebug() << "KeyLayout::KeyLayout()";
    //load();
    TWELTH_ROOT_OF_TWO = 1.05946309436f;
    width = 1000;
    height = 1000;
    leftMargin = 0;
    rightMargin = 0;
    topMargin = 0;
    bottomMargin = 0;
    widthStep = 100;
    heightJump = 100;
    intervStep = 1;
    intervJump = 6.5;
    fMin = 100.f;
    fRoot = 100.f;
    curved = 0.5f;
    curveSegments = 10;
    padding =10;
    borderWidth=3;
    keyButtonList.resize(0);
    setupVoronois();
    setFlag(ItemHasContents, true);

}

KeyLayout::~KeyLayout()
{
}

void KeyLayout::load(){
    qDebug() << "KeyLayout::load()";
    QSettings sts;
    // key layout boundaries
    sts.value("width", width);
    sts.value("height", height);
    sts.value("leftMagin", leftMargin);
    sts.value("rightMargin", rightMargin);
    sts.value("topMargin", topMargin);
    sts.value("bottomMargin", bottomMargin);

    // key to frequency mapping
    sts.value("widthStep", widthStep);
    sts.value("heightJump", heightJump);
    sts.value("intervStep", intervStep);
    sts.value("intervJump", intervJump);
    sts.value("fMin", fMin);
    sts.value("fRoot", fRoot);

    // Button curvature, color, border
    sts.value("curved", curved);
    sts.value("curveSegments", curveSegments);
    sts.value("padding", padding);
    sts.value("borderWidth", borderWidth);
}

void KeyLayout::save(){
    qDebug() << "KeyLayout::save()";
    QSettings sts;
    // key layout boundaries
    sts.setValue("width", width);
    sts.setValue("height", height);
    sts.setValue("leftMagin", leftMargin);
    sts.setValue("rightMargin", rightMargin);
    sts.setValue("topMargin", topMargin);
    sts.setValue("bottomMargin", bottomMargin);

    // key to frequency mapping
    sts.setValue("widthStep", widthStep);
    sts.setValue("heightJump", heightJump);
    sts.setValue("intervStep", intervStep);
    sts.setValue("intervJump", intervJump);
    sts.setValue("fMin", fMin);
    sts.setValue("fRoot", fRoot);

    // Button curvature, color, border
    sts.setValue("curved", curved);
    sts.setValue("curveSegments", curveSegments);
    sts.setValue("padding", padding);
    sts.setValue("borderWidth", borderWidth);
}

void KeyLayout::setupRowsCols(){
    qDebug() << "setupRowsCols()";
    // calculate keys per row and column (todo: show result in gui)
    keysPerRow = static_cast<uint>(
                (width - leftMargin - rightMargin) / static_cast<double>(widthStep)
                );
    keysPerCol = static_cast<uint>(
                (height - topMargin - bottomMargin) / static_cast<double>(heightJump)
                );
}

void KeyLayout::setupFrequencies(){
    qDebug() << "setupFrequencies()";
    // calculate number of steps between lowest and highest key that can be played
    // (minimum freq is fRoot)
    uint stepMax = static_cast<uint>(
                intervJump*keysPerRow + (width - leftMargin - rightMargin)/widthStep
                );
    freqList = std::vector<float>(stepMax);
    // generate all playable frequencies
    {
        //qDebug() << "////freqs://////////";
        float freq = fMin;
        for(uint i=0; i<stepMax; i++){
            freqList.at(i) = freq;
            //qDebug() << freq;
            freq *= powf(2, intervStep/12.f);
        }
    }
}

void KeyLayout::setupKeyButtonList(){
    qDebug() << "setupKeyButtonList()";
    // malloc points - one for each key
    // (there might be several keys to the same frequency)
    numKeyBtns = keysPerRow * keysPerCol;
    keyPositions = static_cast<jcv_point*>(
                malloc( sizeof(jcv_point) * numKeyBtns)
                );
    // calculate key center positions, coloring and save button to indexed list
    float fLeftMargin;
    float fRightMargin;
    float xRel;
    QColor color;
    float x;
    float y;
    int p=0;
    for(uint i=0; i<keysPerCol; i++){
        ///qd << "========================================================================";
        ///qd << "Row: " << i;
        y = heightJump*(0.5f + i) + bottomMargin;
        fLeftMargin = fMin*powf(TWELTH_ROOT_OF_TWO, intervJump*i);
        fRightMargin = fLeftMargin*powf( TWELTH_ROOT_OF_TWO, (width-leftMargin-rightMargin)/widthStep );
        int freqIndex = 0;
        for(float freq: freqList){
            xRel = getRelativePosX(fLeftMargin, fRightMargin, freq);
            // getRelativePos will return -1. if the freq is not in that row
            if(xRel>0.f){
                x = leftMargin + (width - rightMargin - leftMargin)*xRel;
                keyPositions[p].x = x;
                keyPositions[p].y = y;
                // calculate color
                int colVal = 255 - (233*freqIndex)/static_cast<int>(freqList.size());
                switch (freqIndex%12) {
                case 0:  color = QColor(colVal,50,50); break;
                case 1:  color = QColor(0,colVal,0); break;
                case 2:  color = QColor(50,50,colVal); break;
                case 3:  color = QColor(colVal, colVal,0); break;
                case 4:  color = QColor(50,colVal,colVal); break;
                case 5:  color = QColor(colVal,0,colVal); break;
                case 6:  color = QColor(colVal,0,0); break;
                case 7:  color = QColor(50,colVal,50); break;
                case 8:  color = QColor(0,0,colVal); break;
                case 9:  color = QColor(colVal, colVal,50); break;
                case 10: color = QColor(0,colVal,colVal); break;
                case 11: color = QColor(colVal,50,colVal); break;
                }
                keyButtonList.push_back(KeyButton(p, freq, color, x, y, curveSegments));
                p++;
            }
            freqIndex++;
        }
    }
}

float KeyLayout::getRelativePosX(float fXLeftMrgin, float fXRightMargin, float f){
    if(fXLeftMrgin > f || f > fXRightMargin){
        //qd << "Frequency ( f =" << f << ") is not in this row of keys.";
        return -1.;
    } else {
        //qd << "Frequency ( f =" << f << ") is in row ";
        return log2(f/fXLeftMrgin) / log2(fXRightMargin/fXLeftMrgin); // calculate 1-x because switch left right
    }
};

void KeyLayout::setupVoronois(){
    int iTest=14;
    qDebug() << "setupVoronois()";
    setupRowsCols();
    setupFrequencies();
    setupKeyButtonList();

    // create bounding box for voronois decomposition
    jcv_rect* rect = nullptr;//new jcv_rect();
    //rect->min = jcv_point();
    //rect->min.x = 0.;
    //rect->min.y = 0.;
    //rect->max.x = width;
    //rect->max.y = height;

    // create voronoi decomposition of keyPositions which defines the key boundaries
    // qDebug() << "memset()";
    memset(&diagram, 0, sizeof(jcv_diagram));
    jcv_diagram_generate(static_cast<int>(keysPerRow*keysPerCol),
                         static_cast<const jcv_point*>(keyPositions),
                         rect,
                         &diagram);

    // draw key boundaries
    const jcv_site* sites = jcv_diagram_get_sites( &diagram );
    for(uint i=0; i<numKeyBtns; ++i)
    {
        // prepare button edges
        //qDebug() << "prepare button edges (Button " <<i<<")";
        const jcv_site* site = &sites[i];
        const jcv_graphedge* e = site->edges;

        if(padding<=0){
            if(curved<=0)
            {
                // put graphedges into key button rendering vertices

                while(e)
                {
                    QSGGeometry::Point2D vertex;
                    vertex.set(e->pos[0].x,e->pos[0].y);
                    //qDebug() << vertex.x << "," << vertex.y;
                    keyButtonList.at(i).vertices.push_back(vertex);
                    vertex.set(e->pos[1].x,e->pos[1].y);
                    keyButtonList.at(i).vertices.push_back(vertex);
                    //qDebug() << vertex.x << "," << vertex.y;
                    e = e->next;
                }
                //QSGGeometry::Point2D endIsBegin;
                //endIsBegin.set(x0,y0);
                //keyButtonList.at(i).vertices.push_back(endIsBegin);
            } else//(curved>0)
            {
                // prepare round reshape
                while(true){
                    QVector2D v0(e->pos[1].x-e->pos[0].x, e->pos[1].y - e->pos[0].y);
                    QVector2D p0(e->pos[0].x, e->pos[0].y);
                    QVector2D nv0(p0 + v0*(1-curved/2.f));
                    e = e->next;
                    if(e==nullptr){
                        break;
                    }
                    QVector2D v1(e->pos[1].x-e->pos[0].x, e->pos[1].y - e->pos[0].y);
                    QVector2D p1(e->pos[0].x, e->pos[0].y);
                    QVector2D nv1(p1 + v1*(1-curved/2.f));
                    //put bezier-curve points from edge to edge into btn vertices
                    for(uint t=0; t<=keyButtonList[i].curveSegments; t++)
                    {
                        float dt = 1.f/static_cast<float>(keyButtonList[i].curveSegments);
                        QVector2D vertexQV2D;
                        vertexQV2D = Seg2f::bezier(nv0, nv1, p0, t*dt);
                        QSGGeometry::Point2D vertexQSGG2D;
                        vertexQSGG2D.set(vertexQV2D.x(), vertexQV2D.y());
                        keyButtonList[i].vertices.push_back(vertexQSGG2D);
                    }
                }//end while(e)
            }//end else
        } else {//(padding > 0)
            // prepare clipper offset stuff
            if(i==iTest){
                qDebug() << "prepare clipper offset stuff";
            }
            ClipperLib::Path poly;
            // traverse edges of button and add lines to poly
            while( e )
            {
                if(i==iTest){
                    qDebug() << "add line to poly";
                    qDebug() << e->pos[0].x << "," << e->pos[0].y;
                    qDebug() << e->pos[1].x << "," << e->pos[1].y;
                }
                poly << ClipperLib::IntPoint(e->pos[0].x, e->pos[0].y)
                     << ClipperLib::IntPoint(e->pos[1].x, e->pos[1].y);
                e = e->next;
            }
            // do clipper offset stuff
            ClipperLib::ClipperOffset co;
            co.AddPath(poly, ClipperLib::jtRound, ClipperLib::etClosedPolygon);
            ClipperLib::Paths solution;
            co.Execute(solution, -padding);

            if (curved<=0){
                // put graphedges into key button rendering vertices
                if(i==iTest){
                    qDebug() << "put graphedges into key button rendering vertices";
                }
                for(uint u=0; u<solution[0].size(); u++)
                {
                    QSGGeometry::Point2D vertex;
                    vertex.set(solution[0][u].X, solution[0][u].Y);
                    if(i==iTest){
                        qDebug() << "vertex " << u << " ; (x,y)=("<<vertex.x << vertex.y<<")=("<<solution[0][u].X<<","<<solution[0][u].Y<<")" ;
                    }
                    keyButtonList.at(i).vertices.push_back(vertex);
                }
                QSGGeometry::Point2D endIsBegin;
                endIsBegin.set(solution[0][0].X, solution[0][0].Y);
                keyButtonList.at(i).vertices.push_back(endIsBegin);
            } else {
                // add endpoint to solution in order to catch the "ending line"-"beginning line"-edge-case
                solution.push_back(solution.at(0));
                solution.push_back(solution.at(1));
                // prepare round reshape
                if(i==iTest){
                    qDebug() << "prepare round reshape";
                }
                for(uint ii=0, jj=1, kk=2; ii<solution[0].size(); ii++,jj++,kk++){
                    if (kk==solution[0].size()) kk=0;
                    if (jj==solution[0].size()) jj=0;
                    if(i==iTest){
                        qDebug() << "ii,jj,kk=" << ii << "," << jj << "," << kk;
                        qDebug() << "solution[0].size()" << solution[0].size();
                    }
                    QVector2D p0(solution[0][ii].X, solution[0][ii].Y);
                    QVector2D p1(solution[0][jj].X, solution[0][jj].Y);
                    QVector2D p2(solution[0][kk].X, solution[0][kk].Y);

                    QVector2D v0 = p1-p0;
                    QVector2D v1 = p2-p1;

                    QVector2D nv0 = p0 + v0*(1.f-(curved/2.f));
                    QVector2D nv1 = p1 + v1*(curved/2.f);
                    if(i==iTest){
                        qDebug() << "put bezier points to vertex data";
                        qDebug() << "p0=" << p0.x() << "," << p0.y() << "   p1="<< p1.x() << ","<<p1.y();
                    }
                    for(uint t=0; t<=keyButtonList[i].curveSegments; t++)
                    {
                        float dt = 1.f/static_cast<float>(keyButtonList[i].curveSegments);
                        QVector2D vertexQV2D;
                        if(i==iTest){
                            qDebug() << "get point, t=" << t;
                        }
                        vertexQV2D = Seg2f::bezier(nv0, nv1, p1, t*dt);
                        QSGGeometry::Point2D vertexQSGG2D;
                        vertexQSGG2D.set(vertexQV2D.x(), vertexQV2D.y());
                        keyButtonList[i].vertices.push_back(vertexQSGG2D);
                    }
                    // qDebug() << "put bezier points to vertex data - finished";

                }
            }
        }//end else (padding > 0)
    }//end for (uint i=0; i<numKeyBtns; ++i )
    jcv_diagram_free( &diagram );
    qDebug() << "jcv_diagram_free( &diagram );";
}


QSGNode* KeyLayout::updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData*)
{
    QSGNode* node = nullptr;
    qDebug() << "updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData*)";
    if (!oldNode)
    {
        node = new QSGNode;
        //first initialisation
        qDebug() << "first initialisation";
        for(KeyButton kBtnData: keyButtonList)
        {
            QSGGeometryNode* kBtnNode = new QSGGeometryNode;
            QSGGeometry* kBtnGeometry = new QSGGeometry(
                        QSGGeometry::defaultAttributes_Point2D(),
                        kBtnData.vertices.size());
            kBtnGeometry->setLineWidth(borderWidth);
            kBtnGeometry->setDrawingMode(QSGGeometry::DrawLineStrip);
            kBtnNode->setGeometry(kBtnGeometry);
            kBtnNode->setFlag(QSGNode::OwnsGeometry);
            QSGFlatColorMaterial* material = new QSGFlatColorMaterial;
            material->setColor(kBtnData.color);
            kBtnNode->setMaterial(material);
            kBtnNode->setFlag(QSGNode::OwnsMaterial);
            //qDebug() <<"kBtnNode->setFlag(QSGNode::OwnsMaterial);";
            node->appendChildNode(kBtnNode);
        }
    } else
    {
        qDebug() << "second initialisation";
        node = oldNode;
    }
    qDebug() << "=node->childCount()=" << node->childCount();
    for(int i=0; i<node->childCount(); i++){
        QSGGeometryNode* kBtnNode = static_cast<QSGGeometryNode*>(node->childAtIndex(i));
        QSGGeometry* kBtnGeometry = kBtnNode->geometry();
        KeyButton& kBtnData = keyButtonList.at(i);
        //qDebug() << kBtnData.vertices.size() << "i=" << i << "\tkBtnGeometry->allocate("<<kBtnData.vertices.size()<<")";
        kBtnGeometry->allocate(kBtnData.vertices.size());
        QSGGeometry::Point2D* vertices = kBtnGeometry->vertexDataAsPoint2D();
        int k=0;
        for(QSGGeometry::Point2D& vertex: kBtnData.vertices)
        {
            vertices[k] = vertex;
            k++;
        }

    }
    qDebug() << " node->markDirty(QSGNode::DirtyGeometry)";
    node->markDirty(QSGNode::DirtyGeometry);

    return node;
}
