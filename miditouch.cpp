#include "miditouch.h"
#include <QDebug>




//////////////////////////////////////////
/// \brief MidiTouch::MidiTouch
/// \param parent
///
MidiTouch::MidiTouch(QObject* parent)
    : QObject(parent) {
    qDebug() << "/////////////////////////";
    qDebug() << "//TouchPoint intialized//";
}

void MidiTouch::playMidi()
{
    qDebug() << "/////////////////////////////////";
    getPosX();
    getPosY();
    //getVelX();
    //getVelY();
    //getPressure();
    qDebug() << "area width: "<< getArea().width();
    qDebug() << "area height: " << getArea().height();
    qDebug() << "--------------------------------";
    qDebug() << "playMidi";
    qDebug() << "channel:" << 1+pointId;
    cccc channel = (cccc) (pointId + 1);
    if(pressed){
        qDebug() << "isPressed-->noteOn";
        midiOut->noteOn(channel0,key32,vel100);
    } else {
        qDebug() << "isReleased-->channelOff";
        midiOut->allNotesOff(channel0);
    }


}

//////////////////////////////////////////
/// \brief MidiTouch::getPosX
/// \return
///
int MidiTouch::getPosX(){
    qDebug() << "getPosX():" << posX;
    return posX;
}

//////////////////////////////////////////
/// \brief MidiTouch::getPosY
/// \return
///
int MidiTouch::getPosY(){
    qDebug() << "getPosY():" << posY;
    return posY;
}

//////////////////////////////////////////
/// \brief MidiTouch::getVelX
/// \return
///
double MidiTouch::getVelX(){
    qDebug() << "getVelX(): " << velX;
    return  velX;
}

//////////////////////////////////////////
/// \brief MidiTouch::getVelY
/// \return
///
double MidiTouch::getVelY(){
    qDebug() << "getVelY(): " << velY;
    return velY;
}

//////////////////////////////////////////
/// \brief MidiTouch::getArea
/// \return
///
QRectF MidiTouch::getArea()
{
    // qDebug() << "getArea(): " << area;
    return area;
}

//////////////////////////////////////////
/// \brief MidiTouch::getPressure
/// \return
///
double MidiTouch::getPressure()
{
    qDebug() << "getPressure(): " << pressure;
    return  pressure;
}

//////////////////////////////////////////
/// \brief MidiTouch::getPointId
/// \return
///
int MidiTouch::getPointId()
{
    qDebug() << "getPointId(): " << pointId;
    return pointId;
}

//////////////////////////////////////////
/// \brief MidiTouch::isPressed
/// \return
///
bool MidiTouch::isPressed()
{
    qDebug() << "isPressed()" << pressed;
    return pressed;
}

//////////////////////////////////////////
/// \brief MidiTouch::setPosX
/// \param posX
///
void MidiTouch::setPosX(int posX)
{

    //qDebug() << "setPosX( " << posX << " )";
    this->posX = posX;
}

//////////////////////////////////////////
/// \brief MidiTouch::setPosY
/// \param posY
///
void MidiTouch::setPosY(int posY){
    // qDebug() << "setPosY( " << posY << " )";
    this->posY = posY;
}

//////////////////////////////////////////
/// \brief MidiTouch::setVelX
/// \param velX
///
void MidiTouch::setVelX(double velX){
    //qDebug() << "setVelX( " << velX;
    this->velX = velX;
}

//////////////////////////////////////////
/// \brief MidiTouch::setVelY
/// \param velY
///
void MidiTouch::setVelY(double velY){
    //qDebug() << "setVelY( " << velY << " )";
    this->velY = velY;
}

//////////////////////////////////////////
/// \brief MidiTouch::setArea
/// \param area
///
/// Triggers emidiatly after x,y,pressed was set,
/// contains width and height of the touched area
///
void MidiTouch::setArea(QRectF area){
    //qDebug() << "setArea( " << area << " )" ;
    this->area = area;
    // play midi
    playMidi();
}

//////////////////////////////////////////
/// \brief MidiTouch::setPressure
/// \param pressure
///
void MidiTouch::setPressure(double pressure){
    qDebug() << "setPressure( " << pressure << ") ";
    this->pressure = pressure;
}

//////////////////////////////////////////
/// \brief MidiTouch::setPointId
/// \param id
///
void MidiTouch::setPointId(int id)
{
    qDebug() << "setPointId( " << id << " )";
    pointId = id;
}

//////////////////////////////////////////
/// \brief MidiTouch::setPressed
/// \param pressed
/// \return
///
void MidiTouch::setPressed(bool pressed)
{
    // qDebug() << "setPressed( " << pressed << ") ";
    this->pressed = pressed;
    if(!pressed){
        playMidi();
    }
}
