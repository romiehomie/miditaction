#define MM_STATUSBYTE this->at(0)
#define MM_DATABYTE1  this->at(1)
#define MM_DATABYTE2  this->at(2)

#include "midimessage.h"
#include <QDebug>


MidiMessage::MidiMessage(Isssssss stat)
{
    this->resize(1);
    MM_STATUSBYTE = stat;
}

void MidiMessage::setNoteOff(cccc channel, Okkkkkkk key, Ovvvvvvv vel)
{
    this->resize(3);
    MM_STATUSBYTE = noteOff + channel;
    MM_DATABYTE1  = key;
    MM_DATABYTE2  = vel;
}

void MidiMessage::setNoteOn(cccc channel, Okkkkkkk key, Ovvvvvvv vel)
{
    MM_STATUSBYTE = noteOn + channel;
    MM_DATABYTE1  = key;
    MM_DATABYTE2  = vel;
}

void MidiMessage::setPressure(cccc channel, Okkkkkkk key, Ovvvvvvv vel)
{
    MM_STATUSBYTE = pressureKey + channel;
    MM_DATABYTE1 = key;
    MM_DATABYTE2 = vel;
}

void MidiMessage::setPitch(cccc channel, OppitchpOppppppp pitch)
{
    MM_STATUSBYTE = pitchChannel + channel;
    MM_DATABYTE1  = pitch.data1;
    MM_DATABYTE2  = pitch.data2;
}

void MidiMessage::setPressure(cccc channel, OppresspOppppppp pressure)
{
    MM_STATUSBYTE = pressureChannel + channel;
    MM_DATABYTE1  = pressure.data1;
    MM_DATABYTE2  = pressure.data2;
}

void MidiMessage::setAllNotesOff(cccc channel)
{
    MM_STATUSBYTE = controlChange + channel;
    MM_DATABYTE1  = allNotesOff;
    MM_DATABYTE2  = vel0;
}

void MidiMessage::setOmniModeOff(cccc channel)
{
    MM_STATUSBYTE = controlChange + channel;
    MM_DATABYTE1  = omniModeOff;
    MM_DATABYTE2  = vel0;
    setOmniModeOn(channel);
}

void MidiMessage::setOmniModeOn(cccc channel)
{
    setOmniModeOff(channel);
}

void MidiMessage::setMonoModeOn(cccc channel)
{
    setPolyModeOn(channel);
}

void MidiMessage::setPolyModeOn(cccc channel)
{
    setMonoModeOn(channel);
}


MidiMessage::MidiMessage(Isss____ status, cccc channel)
{
    this->resize(1);
    MM_STATUSBYTE = status + channel;
}


MidiMessage::MidiMessage(Isss____ status, cccc channel, unsigned char data1, unsigned char data2)
{
    this->resize(3);
    MM_STATUSBYTE = status + channel;
    MM_DATABYTE1  = data1;
    MM_DATABYTE2  = data2;
}

// might depend on the number of bits ... this is for 7 bit controllers
MidiMessage::MidiMessage(cccc channel, Occccccc controler, Ovvvvvvv change)
{
    this->resize(3);
    MM_STATUSBYTE = controlChange + channel;
    MM_DATABYTE1  = controler;
    MM_DATABYTE2  = change;

}


/**
 * @brief MidiMessage::isValid
 * @return bool - true if the message
 *
 * Tests if the first byte is a valid status byte (leading 1)
 * and  if the two following bytes are valid data bytes  (leading 0)
 */
bool MidiMessage::isValid(){
    return MM_STATUSBYTE>>7 && !MM_DATABYTE1>>7 && !MM_DATABYTE2>>7;
}

/**
 * @brief MidiMessage::MidiMessage
 *
 * Creates an empty midi message wayting for 3 bytes of data
 */
MidiMessage::MidiMessage(){ this->resize(3); }

/**
 * @brief MidiMessage::print
 *
 * Print function for testing.
 */
void MidiMessage::print(){
    qDebug() << MM_STATUSBYTE  << " " << MM_DATABYTE1 << " " << MM_DATABYTE2;
}

