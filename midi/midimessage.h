#ifndef MIDIMESSAGE_H
#define MIDIMESSAGE_H

#include <vector>
#include "RtMidi.h"

#include "midimessagenames.h"

class MidiMessage : public std::vector<unsigned char>
{
private:
    bool isValid();
public:
    MidiMessage();
    MidiMessage(Isssssss systemStatus);
    MidiMessage(Isss____ channelStatus, cccc channel);
    MidiMessage(Isss____ channelStatus, cccc channel, unsigned char data1, unsigned char data2);
    MidiMessage(cccc channel, Occccccc controlChange, Ovvvvvvv change);

    void setNoteOff(cccc channel, Okkkkkkk key, Ovvvvvvv vel);
    void setNoteOn(cccc channel, Okkkkkkk key, Ovvvvvvv vel);
    void setPressure(cccc channel, Okkkkkkk key, Ovvvvvvv vel);

    void setPitch(cccc channel, OppitchpOppppppp pitch);
    void setPressure(cccc channel, OppresspOppppppp pressure);

    // channel mode massages
    void setAllSoundsOff(cccc channel);
    void setResetAllControllers(cccc channel);
    void setLocalControlOn(cccc channel);
    void setLocalControlOff(cccc channel);
    void setAllNotesOff(cccc channel); // 1011cccc
    void setOmniModeOff(cccc channel); // triggers setAllNotesOff()
    void setOmniModeOn(cccc channel);  // triggers setAllNotesOff()
    void setMonoModeOn(cccc channel);  // triggers setPolyModeOff()
    void setPolyModeOn(cccc channel);  // triggers setMonoModeoff()

    //


    void print();
};

#endif // MIDIMESSAGE_H
