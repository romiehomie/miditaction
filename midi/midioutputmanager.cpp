#include "midioutputmanager.h"
#include "midimessage.h"
#include "midimessagenames.h"

// Platform-dependent sleep routines.
#if defined(WIN32)
  #include <windows.h>
  #define SLEEP( milliseconds ) Sleep( (DWORD) milliseconds )
#else // Unix variants
  #include <unistd.h>
  #define SLEEP( milliseconds ) usleep( (unsigned long) (milliseconds * 1000.0) )
#endif

// Platform-dependend availability of virtual midi ports
#if defined(WIN32)
  #define MIDIOUTPUTMANAGER_VIRTUAL_PORT false
#else // Unix variants
  #define MIDIOUTPUTMANAGER_VIRTUAL_PORT true
#endif

MidiOutputManager::MidiOutputManager() : MidiOutputManager(0) {};

MidiOutputManager::MidiOutputManager(unsigned int port){
    virtualPort = MIDIOUTPUTMANAGER_VIRTUAL_PORT;
    midiMsg.resize(3);
    // RtMidiOut constructor
    try {
        midiOut = new RtMidiOut();
    } catch (RtMidiError& e) {
        std::cerr << "Error: RtMidiOut can not be initialized.";
        e.printMessage();
    }

    setPort(port);

    pitchMaxSemiTones=2.;
    pitch = 0.;
}

/**
 * @brief midiOutputManager::~midiOutputManager
 *
 * Cleans up RtMidiOut object.
 */
MidiOutputManager::~MidiOutputManager(){
    midiOut->closePort();
    delete midiOut;
}

void MidiOutputManager::setPort(unsigned int port){
    // check available ports
    if(port < midiOut->getPortCount()) {
        this->port = port;
    } else{
        this->port = -1;
        std::cout << "Midi Output Port not available\n";
    }
}

int MidiOutputManager::getPort(){ return port; }

int MidiOutputManager::getNumbOfPorts(){ return midiOut->getPortCount(); }

void MidiOutputManager::printMidiOutputPorts(){
    std::cout << "\n" << getNumbOfPorts() << " MIDI output sources available." << std::endl;
    for (int i=0; i<getNumbOfPorts(); i++){
        try {
            std::cout << "  Output Port "
                      << i
                      << ": \""
                      << midiOut->getPortName(i)
                      << "\""
                      << std::endl;
        } catch (RtMidiError& e) {
            e.printMessage();
        }
    }
    std::cout << std::endl;
}

void MidiOutputManager::noteOn(cccc channel, Okkkkkkk key, Ovvvvvvv vel)
{
    //std::cout << "Note " << note << " on: channel ?, velocity "<< vel << std::endl;
    //std::vector<unsigned char> message;
    midiMsg.setNoteOn(channel, key, vel);
    midiMsg.print();
    midiOut->sendMessage( &midiMsg );
}

void MidiOutputManager::noteOff(cccc channel, Okkkkkkk key, Ovvvvvvv vel)
{
    midiMsg.setNoteOff(channel, key, vel);
    midiOut->sendMessage( &midiMsg );
}

void MidiOutputManager::keyPressure(cccc channel, Okkkkkkk key, Ovvvvvvv vel)
{
    midiMsg.setPressure(channel, key, vel);
    midiOut->sendMessage( &midiMsg );
}

void MidiOutputManager::allNotesOff(cccc channel)
{
    midiMsg.setAllNotesOff(channel);
    midiOut->sendMessage( &midiMsg );
}

void MidiOutputManager::channelPitch(cccc channel, OppitchpOppppppp pitchData)
{
    midiMsg.setPitch(channel, pitchData);
    midiOut->sendMessage( &midiMsg );
}

void MidiOutputManager::channelPressure(cccc channel, OppresspOppppppp pressure)
{
    midiMsg.setPressure(channel, pressure);
    midiOut->sendMessage( &midiMsg );
}

void MidiOutputManager::openPort(){
    if(port>getNumbOfPorts()){ std::cout << "No Midi Output Ports available. "
                                            "Use loopMidi on Windows to simulate midi-ports.\n"; }
    else             { midiOut->openPort(port); }
}

void MidiOutputManager::openVirtualPort(const std::string portname)
{
    (void) portname;
    std::cout << "Todo: mac linux only... use loopMidi on windows" << std::endl;
}

void MidiOutputManager::closePort()
{
    midiOut->closePort();
}

void MidiOutputManager::testOutput()
{
    std::cout << "Program change: 192, 5" << std::endl;
    midiMsg[0] = 192;
    midiMsg[1] = 5;
    midiOut->sendMessage( &midiMsg );

    std::cout << "Test Control Change: 176, 7, 100 (volume)" << std::endl;
    midiMsg[0] = 176;
    midiMsg[1] = 7;
    midiMsg[2] = 100;
    midiOut->sendMessage( &midiMsg );

    std::cout << "Test Note On: 144, 64, 90" << std::endl;
    noteOn(channel0, key64, vel90);

    SLEEP( 500 );

    std::cout << "Test Note Off: 128, 64, 40" << std::endl;
    noteOff((cccc) 0, (Okkkkkkk) 64, (Ovvvvvvv) 40);

    SLEEP( 100 );
    std::cout << "\nTest pitch bend:" << std::endl;
    std::cout << "Note on: channel 1, middle C, velocity 112" << std::endl;
    noteOn(channel0, key60, vel112);

    SLEEP( 200 );
    std::cout << "Pitchbend 2 Semitones up." << std::endl;
    channelPitch(channel0, OppitchpOppppppp(2.,2.));
    midiMsg.setPitch(channel0, OppitchpOppppppp(2., 2.));
    midiMsg.print();
    midiOut->sendMessage( &midiMsg );
    SLEEP( 200 );
    std::cout << "Pitchbend 2 Semitones down." << std::endl;
    midiMsg.setPitch(channel0, OppitchpOppppppp(-2., 2.));
    midiOut->sendMessage( &midiMsg );
    midiMsg.print();
    SLEEP( 200 );
    std::cout << "Pitchbend a Quartertone down." << std::endl;
    midiMsg.setPitch(channel0, OppitchpOppppppp(0.5, 2.));
    midiMsg.print();
    midiOut->sendMessage( &midiMsg );
    SLEEP( 1000 );

    std::cout << "Note off: channel 1, middle C" << std::endl;
    noteOff(channel0, key60, vel0);

}
