#ifndef MIDIOUTPUTMANAGER_H
#define MIDIOUTPUTMANAGER_H

#include <iostream>
#include <string>
#include <vector>
#include "RtMidi.h"
#include "midimessage.h"
class MidiOutputManager
{
    RtMidiOut* midiOut;
    int port;
    bool virtualPort;
    MidiMessage midiMsg;

public:
    double pitchMaxSemiTones;
    double pitch;

    MidiOutputManager();
    MidiOutputManager(unsigned int port);
    ~MidiOutputManager();

    void setPort(unsigned int port);
    int getPort();

    int getNumbOfPorts();
    bool isPortOpen();

    void openPort();
    void openVirtualPort(const std::string newPortName);
    void closePort();

    void printMidiOutputPorts();


    /////////////////
    /// Messaging ///
    /////////////////

    void noteOn(cccc channel, Okkkkkkk key, Ovvvvvvv vel);
    void noteOn(cccc channel, unsigned char key, unsigned char vel);
    void noteOff(cccc channel, Okkkkkkk key, Ovvvvvvv vel);
    void noteOff(cccc channel, unsigned char key, unsigned char vel);
    void keyPressure(cccc channel, Okkkkkkk key, Ovvvvvvv vel);

    void allNotesOff(cccc channel);

    // 14 bit controller
    void channelPitch(cccc channel, OppitchpOppppppp pitch);
    void channelPressure(cccc channel, OppresspOppppppp pressure);

    // 7 bit controller
    void bankSelect(Ovvvvvvv preset);
    void modulationWheel(Ovvvvvvv amplitude);
    void breath(Ovvvvvvv velocity);
    void undefined1(Ovvvvvvv);
    void foot(Ovvvvvvv velocity);
    void portamentoTime(Ovvvvvvv velocity);
    void dataEntrySlider(Ovvvvvvv entry);
    void volume(Ovvvvvvv amp);
    void balance(Ovvvvvvv balance);
    void undefined2(Ovvvvvvv);
    void pan(Ovvvvvvv pan);
    void expression(Ovvvvvvv exp);
    void effect1(Ovvvvvvv amp);
    void effect2(Ovvvvvvv amp);
    void undefined3(Ovvvvvvv);
    void undefined4(Ovvvvvvv);


    void testOutput();

};


#endif // MIDIOUTPUTMANAGER_H
