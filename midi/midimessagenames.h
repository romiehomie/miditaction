#ifndef MIDIMESSAGENAMES_H
#define MIDIMESSAGENAMES_H

#include <iostream>
#include <cmath>


/**
 *  standard MIDI messages consist of 3 bytes
 *  status   data1    data2
 *  1ssscccc 0xxxxxxx 0xxxxxxx
 *
 *  where the status byte starts with bit 1
 *  and data bytes start with bit 0
 *
 *  sss denotes the 3 status bytes
 *  cccc denotes the 4 channel number bytes
 *
 *  Commonly messages are written as hex code
 *  hh dd dd
 *
 *  where the status byte takes values (hex)100 < hh < (hex)80
 *  and data bytes are restricted to    (hex)80 < dd < (hex)00
 **/

/**
 *  Message Types:
 *
 *  7 bit volume per channel per key
 *  1ssscccc 0kkkkkkk 0vvvvvvv - NoteOn, NoteOff, PressureNote (polyphonic aftertouch)
 *
 *  7 bit volume per channel per controler:
 *  1ssscccc 0ccccccc 0vvvvvvv - Control Changes like
 *
 *  14 bit controller:
 *  1ssscccc 0ppitchp 0ppppppp - Channel Pitch (Pitch Wheel)
 *  1ssscccc 0ppressp 0ppppppp - Channel key pressure
 *
 *  sysExStart    ?Message?    sysExStop
 *  1sssssss ???????? ???????? 1sssssss
 *
 **/

/**
 * @brief The Isss____ ChannelStatus enum
 *
 * Members: noteOn, noteOff, pressureNote, controlChange, programChange, pressureChannel, pitchChannel
 */
enum Isss____{
    ///////////////////////////////  (adding 4 channel bits
    /// channel specific status ///           cccc
    ///////////////////////////////   gives the status byte)
    noteOff        =  8<<4, // 8n
    noteOn         =  9<<4, // 9n
    pressureKey    = 10<<4, // An    (polyphonic aftertouch)
    controlChange  = 11<<4, // Bn
    programChange  = 12<<4, // Cn
    pressureChannel= 13<<4, // Dn
    pitchChannel   = 14<<4, // En
};

/**
 * @brief The Isssssss enum
 *
 * Members: sysExStart, quarterFrame, songPointer, songSelect, tuneRequest, sysExEnd,
 *          timingClock, start, continu, stop, activeSensing, systemReset
 */
enum Isssssss{
    ////////////////////////////////
    /// system specific messages ///
    ////////////////////////////////
    sysExStart     = 240,   // F0       "System Exclusive Messages" are used for device specific data transfer and handshake stuff
    quarterFrame   = 241,   // F1
    songPointer    = 242,   // F2
    songSelect     = 243,   // F3
                            // F4
                            // F5
    tuneRequest    = 246,   // F6
    sysExEnd       = 247,   // F7       "System Exclusive Messages" except for "universal info" the standard only defines how messages begin and end

    /// system real time messages
    timingClock    = 248,   // F8       just a single status byte to be sent 6 times per midi beat
                            // F9
    start          = 250,   // FA       // from song begin
    continu        = 251,   // FB       // from stop position
    stop           = 252,   // FC       // stops song
                            // FD
    activeSensing  = 254,   // FE       // flags active connection (3 times per second)
    systemReset    = 255    // FF       // reset device to initial state
};


/**
 * @brief The Occccccc enum - when a controll change or channel mode message
 *          hex: Bn xx yy / 1101 cccc 0ccccccc 0vvvvvvv
 *        the first data byte
 *          hex:    xx /              0ccccccc
 *        of this message specifies the type of control change or channel mode message
 *
 * Members: bankSelect, modulationWheel, breathControl, undefined1, foot,
 *          portamentorTime, dataEntrySlider, volume, balance, undefined2,
 *          pan, expression, effect1, effect2, undefined3, undefined4,
 *          generalPurpose1, generalPurpose2,... geralPurpose4
 */
enum Occccccc{
    bankSelect      = 0,  // hex: 00
    modulationWheel = 1,  // hex: 01
    breath          = 2,  // hex: 02
    undefined1      = 3,  // hex: 03
    foot            = 4,  // hex: 04
    portamentoTime  = 5,  // hex: 05
    dataEntrySlider = 6,  // hex: 06
    volume          = 7,  // hex: 07
    balance         = 8,  // hex: 08
    undefined2      = 9,  // hex: 09
    pan             = 10, // hex: 0A
    expression      = 11, // hex: 0B
    effect1         = 12, // hex: 0C
    effect2         = 13, // hex: 0D
    undefined3      = 14, // hex: 0E
    undefined4      = 15, // hex: 0F
    generalPurpose1 = 16, // hex: 10
    generalPurpose2 = 17, // hex: 11
    generalPurpose3 = 18, // hex: 12
    generalPurpose4 = 19, // hex: 13
    // 14 (hex) till 1F  undefined
    // 20 (hex) till 3F  LSbyte for 14bit controllers
    allSoundsOff    = 120, // hex 72, v=0!
    resetControllers= 121, // hex 73, v=0 Value must only be zero unless otherwise
                           // allowed in a specific Recommended Practice.
    // Local Control. When Local Control is Off, all devices on a given channel will
    // respond only to data received over MIDI. Played data, etc. will be ignored.
    // Local Control On restores the functions of the normal controllers.
    localControl    = 122, // hex 74, v=0 off v=127 on
    // When an All Notes Off is received, all oscillators will turn off.
    allNotesOff     = 123, // hex 75, v=0!
    omniModeOff     = 124, // hex 76, v=0!
    omniModeOn      = 125, // hex 77, v=0!
    monoModeOn      = 126, // hex 78, v=0!
    polyModeOn      = 127  // hex 79, v=0!


};

// internal mixing of voices can be changed using volume (hex: 07) and pan (hex: 0A) channel controllers

// RPN and NRPN
// registered and non registered parameter numbers are specific control change messages
// that allow the controll of the internal parameters of a voice patch
// (any aspect of a voice
//  e.g. velocity sensitivity of an envelope generator, range of modulation or bending, etc
//  can be modified remotely via these messages
// RPNs are universal and should be registered with the MMA while NRPNs can be manufacturer specific

enum RegisteredParameterNumber{
    pitchBendSensitivity = 0, // hex: 00 00
    fineTuning = 1,           // hex: 00 01
    coarseTuning = 2,         // hex: 00 02
    tuningProgramSelect = 3,  // hex: 00 03
    tuningBankSelect = 4,     // hex: 00 04
    CancelParameterNumber = (7+16)*(128+1) // hex: 7F 7F
};

/**
 * @brief The cccc (channel number) enum  -
 *
 * Members: channel0, channel1,... channel15
 */
enum cccc{
    channel0  = 0,
    channel1  = 1,
    channel2  = 2,
    channel3  = 3,
    channel4  = 4,
    channel5  = 5,
    channel6  = 6,
    channel7  = 7,
    channel8  = 8,
    channel9  = 9,
    channel10 = 10,
    channel11 = 11,
    channel12 = 12,
    channel13 = 13,
    channel14 = 14,
    channel15 = 15
};

/**
 * @brief The OxxxxxxxOxxxxxxx (14 bit data) class.
 * Overwhelming 14bit resolution is reserved for channel-pitch-bend and channel-pressure
 * Why this high? Wtf
 */
class OxxxxxxxOxxxxxxx{
public:
    unsigned char data1;
    unsigned char data2;

    OxxxxxxxOxxxxxxx(unsigned int n){
        if(n>16383 /*=2^7*/){
            data1 = 127;
            data2 = 127;
            std::cerr << "14 bit data was generated from number with more than 14 bit.\n";
            std::cerr << "Use numbers n<16384. n was " << n << ". Chose maximum.\n";
        }
        data2 = n>>7;
        data1 = n - (data2<<7);
    }

    unsigned int getValue(){
        return (data2<<7) + data1;
    }
};

/**
 * @brief The PitchData class
 *
 */
class OppitchpOppppppp : public OxxxxxxxOxxxxxxx{

public:

    /**
     * @brief PitchData
     * @param f0
     * @param df
     *
     * The standard pitch data interpretation will pitch down x semitones for the minimum
     * n = 0
     * and pitch up x semitones for the maximum
     * n = 2^7-1 = 16383
     *
     * Standard is pitchMaxSemiTones = x = 2
     *
     * f0+df = f0`* (2^(x/12.) ) ^ ( (n-16384/2)/16384 = f0* 2^( (n-8192)*x/12/16383 )
     * =>
     * log2( 1+df/f0 ) = (n-8192)x/12/16383
     * =>
     * n = 8192 + 12/x * 16383 * log2( 1 + df/f0 )
     */
    OppitchpOppppppp(float f0, float df, double pitchMaxSemiTones)
    : OxxxxxxxOxxxxxxx( floor(  8192*(1 + 12/pitchMaxSemiTones * log2(1+df/f0))  ) ){};


    /**
     * @brief PitchData
     * @param semitones
     * @param pitchMaxSemiTones
     *
     * f0+df = f0 * (2^(1/12))^ ( x*(n-8192)/16383 )
     *
     * the last exponent gives the number of semitones
     *
     * semitones = x*(n-8192)/16383
     * => n = 16383*semitones/x + 8192
     */
    OppitchpOppppppp(float semitones, double pitchMaxSemiTones)
    : OxxxxxxxOxxxxxxx(floor(8191.5*semitones/pitchMaxSemiTones + 8192) ){
    }

};

/**
 * @brief The OppresspOppppppp class
 */
class OppresspOppppppp : public OxxxxxxxOxxxxxxx{
public:
    OppresspOppppppp(unsigned int n) : OxxxxxxxOxxxxxxx(n){}
};

/**
 * @brief The Okkkkkkk enum
 *
 * Members: key0, key1,... key127
 */
enum Okkkkkkk{
    key0  = 0,      key1  = 1,      key2  = 2,      key3  = 3,
    key4  = 4,      key5  = 5,      key6  = 6,      key7  = 7,
    key8  = 8,      key9  = 9,      key10 = 10,     key11 = 11,
    key12 = 12,     key13 = 13,     key14 = 14,     key15 = 15,
    key16 = 16,     key17 = 17,     key18 = 18,     key19 = 19,
    key20 = 20,     key21 = 21,     key22 = 22,     key23 = 23,
    key24 = 24,     key25 = 25,     key26 = 26,     key27 = 27,
    key28 = 28,     key29 = 29,     key30 = 30,     key31 = 31,
    key32 = 32,     key33 = 33,     key34 = 34,     key35 = 35,
    key36 = 36,     key37 = 37,     key38 = 38,     key39 = 39,
    key40 = 40,     key41 = 41,     key42 = 42,     key43 = 43,
    key44 = 44,     key45 = 45,     key46 = 46,     key47 = 47,
    key48 = 48,     key49 = 49,     key50 = 50,     key51 = 51,
    key52 = 52,     key53 = 53,     key54 = 54,     key55 = 55,
    key56 = 56,     key57 = 57,     key58 = 58,     key59 = 59,
    key60 = 60,     key61 = 61,     key62 = 62,     key63 = 63,
    key64 = 64,     key65 = 65,     key66 = 66,     key67 = 67,
    key68 = 68,     key69 = 69,     key70 = 70,     key71 = 71,
    key72 = 72,     key73 = 73,     key74 = 74,     key75 = 75,
    key76 = 76,     key77 = 77,     key78 = 78,     key79 = 79,
    key80 = 80,     key81 = 81,     key82 = 82,     key83 = 83,
    key84 = 84,     key85 = 85,     key86 = 86,     key87 = 87,
    key88 = 88,     key89 = 89,     key90 = 90,     key91 = 91,
    key92 = 92,     key93 = 93,     key94 = 94,     key95 = 95,
    key96 = 96,     key97 = 97,     key98 = 98,     key99 = 99,
    key100= 100,    key101= 101,    key102= 102,    key103= 103,
    key104= 104,    key105= 105,    key106= 106,    key107= 107,
    key108= 108,    key109= 109,    key110= 110,    key111= 111,
    key112= 112,    key113= 113,    key114= 114,    key115= 115,
    key116= 116,    key117= 117,    key118= 118,    key119= 119,
    key120= 120,    key121= 121,    key122= 122,    key123= 123,
    key124= 124,    key125= 125,    key126= 126,    key127= 127
};

/**
 * @brief The Ovvvvvvv enum
 *
 * Members: vel0, vel1,... vel127
 */
enum Ovvvvvvv{
    vel0  = 0,      vel1  = 1,      vel2  = 2,      vel3  = 3,
    vel4  = 4,      vel5  = 5,      vel6  = 6,      vel7  = 7,
    vel8  = 8,      vel9  = 9,      vel10 = 10,     vel11 = 11,
    vel12 = 12,     vel13 = 13,     vel14 = 14,     vel15 = 15,
    vel16 = 16,     vel17 = 17,     vel18 = 18,     vel19 = 19,
    vel20 = 20,     vel21 = 21,     vel22 = 22,     vel23 = 23,
    vel24 = 24,     vel25 = 25,     vel26 = 26,     vel27 = 27,
    vel28 = 28,     vel29 = 29,     vel30 = 30,     vel31 = 31,
    vel32 = 32,     vel33 = 33,     vel34 = 34,     vel35 = 35,
    vel36 = 36,     vel37 = 37,     vel38 = 38,     vel39 = 39,
    vel40 = 40,     vel41 = 41,     vel42 = 42,     vel43 = 43,
    vel44 = 44,     vel45 = 45,     vel46 = 46,     vel47 = 47,
    vel48 = 48,     vel49 = 49,     vel50 = 50,     vel51 = 51,
    vel52 = 52,     vel53 = 53,     vel54 = 54,     vel55 = 55,
    vel56 = 56,     vel57 = 57,     vel58 = 58,     vel59 = 59,
    vel60 = 60,     vel61 = 61,     vel62 = 62,     vel63 = 63,
    vel64 = 64,     vel65 = 65,     vel66 = 66,     vel67 = 67,
    vel68 = 68,     vel69 = 69,     vel70 = 70,     vel71 = 71,
    vel72 = 72,     vel73 = 73,     vel74 = 74,     vel75 = 75,
    vel76 = 76,     vel77 = 77,     vel78 = 78,     vel79 = 79,
    vel80 = 80,     vel81 = 81,     vel82 = 82,     vel83 = 83,
    vel84 = 84,     vel85 = 85,     vel86 = 86,     vel87 = 87,
    vel88 = 88,     vel89 = 89,     vel90 = 90,     vel91 = 91,
    vel92 = 92,     vel93 = 93,     vel94 = 94,     vel95 = 95,
    vel96 = 96,     vel97 = 97,     vel98 = 98,     vel99 = 99,
    vel100= 100,    vel101= 101,    vel102= 102,    vel103= 103,
    vel104= 104,    vel105= 105,    vel106= 106,    vel107= 107,
    vel108= 108,    vel109= 109,    vel110= 110,    vel111= 111,
    vel112= 112,    vel113= 113,    vel114= 114,    vel115= 115,
    vel116= 116,    vel117= 117,    vel118= 118,    vel119= 119,
    vel120= 120,    vel121= 121,    vel122= 122,    vel123= 123,
    vel124= 124,    vel125= 125,    vel126= 126,    vel127= 127
};

#endif // MIDIMESSAGENAMES_H
