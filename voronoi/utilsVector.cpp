#include "utilsVector.h"

QVector2D Seg2f::intersectLines_noParallel(const Seg2f &S0, const Seg2f &S1)
{
    const QVector2D s(S1.p[0] - S0.p[0]);
    const QVector2D t(S0.p[1] - S0.p[0]);
    const QVector2D u(S1.p[0] - S1.p[1]);

    const float det = vCrossZ(t, u);
    const float detA = vCrossZ(s, u);

    const float alpha = detA / det;

    return S0.p[0] + t * alpha;
}

float Seg2f::vDot(const QVector2D &a, const QVector2D &b)
{
    return a.x() * b.x() + a.y() * b.y();
}

QVector2D Seg2f::bezier(const QVector2D &s, const QVector2D &e, const QVector2D &c, const float t)
{
    QVector2D q0 =  s +   (c - s) * t;
    QVector2D q1 =  c +   (e - c) * t;
    return q0 + (q1 - q0) * t;
}

QVector2D Seg2f::vRot90(const QVector2D v) {
    QVector2D res;
    res.setX(v.y());
    res.setY(-v.x());
    return res;
}

float Seg2f::vCrossZ(const QVector2D &a, const QVector2D &b)
{
    return a.x() * b.y() - a.y() * b.x();
}
