#ifndef LOCALVORONOIS_H
#define LOCALVORONOIS_H
#include <QSGGeometry>
#include <vector>

class LocalVoronois
{
public:
    LocalVoronois();
    static std::vector<QSGGeometry::Point2D> boundary(
            QSGGeometry::Point2D center,
            std::vector<QSGGeometry::Point2D> neighbours
            )
    {
        std::vector<QSGGeometry::Point2D> edges(neighbours.size());

        return edges;
    }
};

#endif // LOCALVORONOIS_H
