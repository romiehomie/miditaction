#ifndef UTILSVECTOR_H
#define UTILSVECTOR_H

#include <QSGGeometry>
#include <QVector2D>

/**
 * @brief The Seg2f class
 *
 * Util class for the segments of button border curves.
 */
struct Seg2f
{
    QVector2D p[2];         // x and y value of the vector
    bool valid = false;     // will be set true if properly initialized

    /**
     * @brief Seg2f
     *
     * Constructor
     */
    Seg2f() {}

    /**
     * @brief Seg2f
     * @param a - x value
     * @param b - y value
     *
     * Constructor
     */
    Seg2f(const QVector2D &a, const QVector2D &b){
        p[0] = a;
        p[1] = b;
        valid = true;
    }
    void set(const QVector2D &a,const QVector2D &b){ *this = Seg2f(a,b); }

    /**
     * @brief isValid
     * @return true - if the Segment was initialized
     */
    bool isValid() { return valid; }

    /**
     * @brief Seg2f::intersectLines_noParallel
     * @param S0 - first segment
     * @param S1 - second segment
     * @return the intersection of S0 and S1
     */
    static QVector2D intersectLines_noParallel(const Seg2f &S0, const Seg2f &S1);

    /**
     * @brief Seg2f::vCrossZ
     * @param a
     * @param b
     * @return z component of vector (or cross) product of a and b
     */
    static float vCrossZ(const QVector2D &a, const QVector2D &b);

    /**
     * @brief Seg2f::vDot
     * @param a
     * @param b
     * @return scalar product of a and b
     */
    static float vDot(const QVector2D &a, const QVector2D &b);

    /**
     * @brief Seg2f::vRot90
     * @param v
     * @return the vector v rotated by 90°
     */
    static QVector2D vRot90(const QVector2D v);

    /**
     * @brief Seg2f::bezier
     * @param s - start point of curve
     * @param e - end point of curve
     * @param c - curvature param of curve (c=0 gives a straight line)
     * @param t - running variable of bezier-parametrisation
     * @return gives the bezier curve at param t, starting at s, ending at e with curvature c
     */
    static QVector2D bezier(const QVector2D &s, const QVector2D &e, const QVector2D &c, const float t);
};













#endif // UTILSVECTOR_H
